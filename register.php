<?php session_start(); ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Register Now</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css1/bootstrap.min.css">
    <link rel="stylesheet" href="css1/font-awesome.min.css">
    <link rel="stylesheet" href="css1/owl.carousel.css">
    <link rel="stylesheet" href="css1/owl.theme.css">
    <link rel="stylesheet" href="css1/owl.transitions.css">
    <link rel="stylesheet" href="css1/animate.css">
    <link rel="stylesheet" href="css1/normalize.css">
    <link rel="stylesheet" href="css1/main.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css1/responsive.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

    <div class="error-pagewrap">
        <div class="error-page-int">
            <div class="text-center custom-login">
                <h3>Register Now</h3>

            </div>
            <div class="content-error">
                <div class="hpanel">
                    <div class="panel-body">
                        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="POST">
                            <div class="row">
                                <div class="form-group col-lg-12">
                                    <label>FirstName</label>
                                    <input type="text" name="fn" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>LastName</label>
                                    <input type="text" name="ln" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Username</label>
                                    <input type="text" name="un" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Email</label>
                                    <input type="email" name="en" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Contact</label>
                                    <input type="text" name="cn" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Password</label>
                                    <input type="password" name="ps" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Confirm password</label>
                                    <input type="password" name="cps" class="form-control" required>
                                </div>

                            </div>
                            <div class="text-center">
                                <button class="btn btn-success loginbtn" name="submit">Register</button>
                                <p style="margin-bottom:0px; margin-top:8px;">Already have an account?</p>
                                <a class="btn btn-default btn-block" style="margin-top: 0;" href="login.php">Login</a>

                            </div>
                            <div class="alert alert-success" style="margin-top:7px; display:none;" id="success" >
                                <strong>Alert!</strong>Data Inserted</a>.
                            </div>
                            <div class="alert alert-info"  style="margin-top:7px; display:none;" id="info">
                                <strong>Error!</strong> Email or Username Taken</a>.
                            </div>
                            <div class="alert alert-danger"  style="margin-top:7px; display:none;" id="danger">
                                <strong>Alert!</strong> Password Not Matched</a>.
                            </div>
                            <div class="alert alert-warning"  style="margin-top:7px; display:none;" id="warning">
                                <strong>Error!</strong> data not inserted</a>.
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery-price-slider.js"></script>
    <script src="js/jquery.meanmenu.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>

</html>

<?php
include 'connection.php';
if(isset($_POST['submit'])){
$firstname=$_POST['fn'];
$lastname=$_POST['ln'];
$username=$_POST['un'];
$email=$_POST['en'];
$contact=$_POST['cn'];
$password=$_POST['ps'];
$cpassword=$_POST['cps'];

$selectquery="select * from registration_student where username='$username' or email='$email' ";
$query=mysqli_query($con,$selectquery);

$check=mysqli_num_rows($query);

if ($check>0) {
    ?><script>
   
    document.getElementById("info").style.display="block";


</script><?php
}else{
    if ($password === $cpassword) {
        $insertquery="insert into registration_student (firstname,lastname,username,email,contact,password,cpassword) values ('$firstname','$lastname','$username','$email','$contact','$password','$cpassword')";

        $iquery=mysqli_query($con,$insertquery);

        if ($iquery) {

            $subject="Activation Mail";
            $body="hii click here to activate your mail";
            $sender="From: coderbest01@gmail.com";


            if (mail($email,$subject,$body,$sender)) {
                ?><script>

                alert("Congratulations!! Registration Successful  ");
                window.location = "login.php";
                
                </script><?php
            }else{
                ?><script>

                alert("mail not send  ");
                // window.location = "login.php";
                
                </script><?php
            }
          
        }else{
            ?><script>
document.getElementById("warning").style.display="block";

</script><?php
        }
    }else{
        ?><script>
document.getElementById("danger").style.display="block";

</script><?php
    }
}





}
?>