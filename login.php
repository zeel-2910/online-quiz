<?php
session_start();
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css1/bootstrap.min.css">
    <link rel="stylesheet" href="css1/font-awesome.min.css">
    <link rel="stylesheet" href="css1/owl.carousel.css">
    <link rel="stylesheet" href="css1/owl.theme.css">
    <link rel="stylesheet" href="css1/owl.transitions.css">
    <link rel="stylesheet" href="css1/animate.css">
    <link rel="stylesheet" href="css1/normalize.css">
    <link rel="stylesheet" href="css1/main.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css1/responsive.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

    <div class="error-pagewrap">
        <div class="error-page-int">
            <div class="text-center m-b-md custom-login">
                <h3>LOGIN FORM</h3>

            </div>
            <div class="content-error">
                <div class="hpanel">
                    <div class="panel-body">
                        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']);   ?>" method="POST">
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input type="text" placeholder="you@gmail.com" title="Please enter you username"
                                    required="" value="" name="email" id="username" class="form-control">

                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******"
                                    required="" value="" name="password" id="password" class="form-control">

                            </div>

                            <button class="btn btn-success btn-block loginbtn" name="submit1">Login</button>
                            <p style="margin-bottom:0px; margin-top:8px;"></p>
                            <a class="btn btn-default btn-block" href="register.php">Register</a>

                            <div class="alert alert-success" style="margin-top:7px; display:none;" id="success">
                                <strong>!</strong>Connection Successful</a>.
                            </div>
                            <div class="alert alert-info" style="margin-top:7px; display:none;" id="info">
                                <strong>Alert!</strong> Email or Password</a>.
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery-price-slider.js"></script>
    <script src="js/jquery.meanmenu.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>


</body>

</html>

<?php
include 'connection.php';
if (isset($_POST['submit1'])) {


$email=$_POST['email'];
$password=$_POST['password'];

$selectquery="select * from registration_student where email='$email' and password='$password' ";
$query=mysqli_query($con,$selectquery);

$emailcount=mysqli_num_rows($query);
//  header("location:login.php");
        if ($emailcount) {
           
            // window.location="login.php";
            $_SESSION["email"]=$_POST["email"];
            ?><script>
window.location = "select_exam.php";
</script><?php
        }else {
            // $_SESSION["username"]=$_POST["username"];
            ?><script>
// window.location="login.php";
// echo 'window.location.reload();';
// window.location.reload();
document.getElementById("info").style.display = "block";
// window.location="login.php";
// echo 'window.location.reload();';
</script><?php
            
        
        }      
}
?>