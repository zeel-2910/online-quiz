<?php include 'header.php';
include '../connection.php' ?>


<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <form action="" method="POST">

                        <div class="card-body">
                            <div class="col-lg-6">
                                <div class="card">
                                    <!-- <form action="" method="POST"> -->
                                    <div class="card-header"><strong>Add Exam Category</strong></div>

                                    <div class="card-body card-block">
                                        <div class="form-group"><label for="company" class=" form-control-label">New
                                                Exam
                                                Category</label><input type="text" id="company"
                                                placeholder="New Exam Category" name="examcategory" class="form-control"
                                                required>
                                        </div>
                                        <div class="form-group"><label for="vat" class=" form-control-label">Exam Time
                                                In
                                                Minutes</label><input type="text" name="examtime" id="vat"
                                                placeholder="Exam Time In Minutes" class="form-control" required></div>
                                        <div class="form-group">
                                            <input type="submit" name="submit" value="Add Exam" class="btn btn-success">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <strong class="card-title">Exam Categories</strong>
                                </div>
                                <div class="card-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Exam Name</th>
                                                <th scope="col">Exam Time</th>
                                                <th scope="col">Edit</th>
                                                <th scope="col">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $res=mysqli_query($con,"select * from exam_category");
                                            $count=0;                                            
                                            while($row=mysqli_fetch_assoc($res)){
                                            $count=$count+1;
                                                ?>
                                                <tr>
                                                <th scope="row"><?php echo $count; ?></th>
                                                <td><?php echo $row['examcategory'];  ?></td>
                                                <td><?php echo $row['examtime']; ?></td>
                                                <td><a href="edit.php?id=<?php echo $row['id']?>">Edit</a</td>
                                                <td><a href="delete.php?id=<?php echo $row['id']?>">Delete</a></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                            
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>




    </div>



</div>





<?php

include 'footer.php';
?>

<?php
if (isset($_POST['submit'])) {
   $_POST['examtime']; $examcategory=$_POST['examcategory'];
    $examtime=

    $insertquery="insert into exam_category (examcategory,examtime) values ('$examcategory','$examtime')";
    $query=mysqli_query($con,$insertquery);

    if ($query) {
    ?> <script>
window.location.href=window.location.href
</script><?php
    }else {
        ?> <script>
alert("inserted")
</script><?php
    }
}

?>