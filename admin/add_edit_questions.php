<?php include 'header.php';
include '../connection.php';





?>


<div class="breadcrumbs">
    <div class="col-sm-8">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Select exam category for Add and Edit questions</h1>
            </div>
        </div>
    </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <form action="" method="POST">

                        <div class="card-body">

                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Exam Categories</strong>
                                    </div>
                                    <div class="card-body">
                                        <table class="table">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Exam Name</th>
                                                    <th scope="col">Exam Time</th>
                                                    <th scope="col">Select</th>
                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                            $res=mysqli_query($con,"select * from exam_category");
                                            $count=0;                                            
                                            while($row=mysqli_fetch_assoc($res)){
                                            $count=$count+1;
                                                ?>
                                                <tr>
                                                    <th scope="row"><?php echo $count; ?></th>
                                                    <td><?php echo $row['examcategory'];  ?></td>
                                                    <td><?php echo $row['examtime']; ?></td>
                                                    <td><a href="add_question.php?id=<?php echo $row['id']?>">Select</a></td>
                                                   
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>




    </div>



</div>





<?php

include 'footer.php';
?>